package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)
open class NotFoundInPriceList(message: String) : Exception(message)

fun main() {

    println("Количество овощей в корзине - ${countVegetables(userCart)}")
    try {
        println("Стоимость корзины - ${cartCostWithDiscount(userCart)}")
    } catch (e: NotFoundInPriceList){
        println("Не удалось посчитать стоимость корзины по причине: ${e.message}")
    }
}

fun countVegetables(userCart: MutableMap<String, Int>):Int{
    var veggiesCount = 0
    userCart.forEach{
        if (vegetableSet.contains(it.key)) veggiesCount+=it.value
    }
    return veggiesCount
}

fun cartCostWithDiscount(userCart: MutableMap<String, Int>):Double{
    var cost = 0.0
    for((key,value) in userCart){
        try{
            val positionInCartCost =  prices.getValue(key)*value
            cost += if(discountSet.contains(key)) positionInCartCost*(1- discountValue) else positionInCartCost
        }catch(e:NoSuchElementException){
            throw NotFoundInPriceList("Предмет $key не найден в прайс-листе")
        }
    }
    return cost
}
