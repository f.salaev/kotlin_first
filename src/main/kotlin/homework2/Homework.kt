package homework2

fun main() {
    val lemonade = 18500
    val pinacolada: Short = 200
    val whiskey: Byte = 50
    val fresh = 3000000000L
    val cola = 0.5f
    val ale = 0.666666667
    val signatureDrink = "Что-то авторское!"

    println("Заказ - ‘$lemonade мл лимонада’ готов!")
    println("Заказ - ‘$pinacolada мл пина колады’ готов!")
    println("Заказ - ‘$whiskey мл виски’ готов!")
    println("Заказ - ‘$fresh капель фреша’ готов!")
    println("Заказ - ‘$cola литра колы’ готов!")
    println("Заказ - ‘$ale литра эля’ готов!")
    println("Заказ - ‘$signatureDrink’ готов!")
}