package homework6

//write you classes here

abstract class Animal(){
    abstract val name: String
    abstract var height: Int
    abstract var weight: Int
    protected abstract val foodPreferences: Array<String>
    var satiety: Int = 0
    fun eat (food:String){
        if (foodPreferences.contains(food)) satiety++
    }
}

class Lion(override val name: String,
           override var height: Int,
           override var weight: Int) : Animal(){
    override val foodPreferences: Array<String> = arrayOf("мясо","зебра","телятина")
}


class Tiger(override val name: String,
            override var height: Int,
            override var weight: Int): Animal(){
    override val foodPreferences: Array<String> = arrayOf("кабан","оленина","рыба")
}

class Hippo(override val name: String,
            override var height: Int,
            override var weight: Int): Animal(){
    override val foodPreferences: Array<String> = arrayOf("трава","мясо")
}

class Wolf(override val name: String,
           override var height: Int,
           override var weight: Int): Animal(){
    override val foodPreferences: Array<String> = arrayOf("оленина","кабан","баранина","зайчатина")
}

class Giraffe(override val name: String,
              override var height: Int,
              override var weight: Int): Animal(){
    override val foodPreferences: Array<String> = arrayOf("листья","трава")
}

class Elephant(override val name: String,
               override var height: Int,
               override var weight: Int): Animal(){
    override val foodPreferences: Array<String> = arrayOf("листья","трава", "фрукт")
}

class Chimpanzee(override val name: String,
                 override var height: Int,
                 override var weight: Int): Animal(){
    override val foodPreferences: Array<String> = arrayOf("фрукт","трава","насекомое")
}

class Gorilla(override val name: String,
              override var height: Int,
              override var weight: Int): Animal(){
    override val foodPreferences: Array<String> = arrayOf("бамбук","лианы","сельдерей")
}


fun main(){
    val lion = Lion("Симба", 120, 220)
    val tiger = Tiger ("Шерхан", 110, 250)
    val hippo = Hippo ("Глория", 165, 1400)
    val wolf = Wolf ("Акела", 80, 65)
    val giraffe = Giraffe ("Марти", 580, 1100)
    val elephant = Elephant ("Бизнес", 350,5000)
    val chimpanzee = Chimpanzee ("Чита", 120, 40)
    val gorilla = Gorilla ("Терк", 150, 365)

    var animals: Array<Animal> = arrayOf(lion,tiger,hippo,wolf,giraffe,elephant,chimpanzee,gorilla)
    val dailyRation = arrayOf("мясо","рыба","баранина","листья","трава","бамбук","фрукт")

    feedAnimals(animals,dailyRation)

    for(animal in animals)
        println("Сытость ${animal.name} равна ${animal.satiety}")
}

//TODO: Homework2 write your function here

fun feedAnimals(animals: Array<Animal>, dailyRation: Array<String> ){
    for(animal in animals){
        for (food in dailyRation) {
            animal.eat(food)
        }
    }
}