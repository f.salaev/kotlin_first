package homework9

import java.lang.IllegalArgumentException

open class IncorrectShotException (message: String): Exception(message)
open class IncorrectShipLocationException (message:String): Exception(message)


const val dimensions = 4
const val startSymbol = 'A'
const val endSymbol = 'A'+dimensions-1

enum class State(val visual: String){
    EMPTY(" "),
    MISSED("·"),
    INTACT("▢"),
    DAMAGED("◫"),
    DESTROYED("⊠")
}

data class Square(val location: String):Comparable<Square>{

    init{
        require(location.length == 2)
        {"Введено больше символов, чем необходимо"}

        require(location[0] in startSymbol..endSymbol)
        {"Первый символ должен быть от $startSymbol до $endSymbol"}

        require(location[1].digitToInt() in 1..dimensions)
        {"Второй символ должен быть от 1 до $dimensions"}
    }

    val x = location[1].digitToInt()-1
    val y = location[0] - startSymbol

    override fun compareTo(other: Square) = compareValuesBy(this, other){it.location}
}

class Ship(val squares: MutableList<Square>){
    var health = squares.size

    init{
        if(squares.size > 1){
            squares.sort()
            if (squares.toSet().size != squares.size) throw IncorrectShipLocationException("В квадратах корабля есть дубликарты")

            var horizontal:Boolean
            if (squares[0].x == squares[1].x && squares[1].y-squares[0].y ==1) horizontal = true
            else if (squares[0].y == squares[1].y && squares[1].x-squares[0].x ==1) horizontal = false
            else throw IncorrectShipLocationException("Палубы корабля должны следовать друг за другом на одной прямой")

            if (squares.size >2)
            for(i in 2 until squares.size){
                if (horizontal) {
                    if (squares[i].x != squares[i-1].x || squares[i].y-squares[i-1].y !=1)
                        throw IncorrectShipLocationException("Палубы корабля должны следовать друг за другом на одной прямой")
                }
                else if (squares[i].y != squares[1-1].y && squares[i].x-squares[i-1].x !=1)
                    throw IncorrectShipLocationException("Палубы корабля должны следовать друг за другом на одной прямой")
            }
        }
    }



}

class Grid() {

    private val aliveShips: MutableSet<Ship> = mutableSetOf()
    private val squareToShips: MutableMap<String, Ship> = mutableMapOf ()
    private val gridArray: Array<Array<State>> = Array(dimensions) { Array(dimensions) { State.EMPTY } }


    fun addShip (ship: Ship){
        for (square in ship.squares){

            for (i in -1..1){
                for(j in -1..1){
                    val iterationX = square.x+i
                    val iterationY = square.y+j
                    if (iterationX in 0 until dimensions && iterationY in 0 until dimensions){
                        if (gridArray[iterationX][iterationY] == State.INTACT) throw IncorrectShipLocationException ("Слишком близко к уже добавленным кораблям")
                    }
                }
            }
        }

        for (square in ship.squares){
            gridArray[square.x][square.y] = State.INTACT
            squareToShips[square.location] = ship

        }
        aliveShips.add(ship)
    }

    fun visualizePlayerGrid(){
        for(row in gridArray){
            println (row.joinToString(prefix = "|", postfix = "|", separator = "|" ){" "+ it.visual+" "})
        }
        println()
    }

    fun visualizeEnemyGrid(){
        for(row in gridArray){
            print("|")
            for (element in row){
                if (element != State.INTACT)
                    print(" ${element.visual} |")
                else print(" " + State.EMPTY.visual+" |")
            }
            println()
        }
        println()
    }


    //Если попали в корабль, то помечаем квадрат как damaged или destroyed. Если не попали, то как missed.
    //Если уже стреляли по указанному квадрату, то кидаем IncorrectShotException
    fun markAShot(square: Square): State{
        when(gridArray[square.x][square.y]){
            State.INTACT -> {
                val ship = squareToShips.getValue(square.location)
                ship.health-=1
                if (ship.health == 0) {
                    aliveShips.remove(ship)
                    for(loc in ship.squares){
                        gridArray[loc.x][loc.y] = State.DESTROYED
                    }
                    return State.DESTROYED
                }
                else{
                    gridArray[square.x][square.y] = State.DAMAGED
                    return State.DAMAGED
                }
            }
            State.EMPTY -> {
                gridArray[square.x][square.y] = State.MISSED
                return State.MISSED
            }
            State.MISSED, State.DAMAGED, State.DESTROYED -> {
                throw IncorrectShotException("Вы уже стреляли по этому квадрату")
            }
        }
    }

    //Проверяем, остались ли целые корабли на поле
    fun isGridClear():Boolean{
        return aliveShips.size==0
    }
}

class Player(val name: String, val playerGrid: Grid, val enemyGrid: Grid){

    fun shoot (square: Square): State{

        var result: State
        try {
            result = enemyGrid.markAShot(square)
        }catch (e: IncorrectShotException){
            throw e
        }
        return result
    }

    fun visualizeGrids(){
        println("Поле игрока $name")
        playerGrid.visualizePlayerGrid()
        println("Поле соперника")
        enemyGrid.visualizeEnemyGrid()
    }

    fun addShip(ship: Ship){
        playerGrid.addShip(ship)
    }

    // Проверяем, остались ли корабли у противника
    fun isEnemyDead():Boolean{
        return enemyGrid.isGridClear()
    }

}

class Game(player1Name: String, player2Name: String){
    val player1Grid = Grid()
    val player2Grid = Grid()
    val player1 = Player(player1Name,player1Grid,player2Grid)
    val player2 = Player(player2Name,player2Grid,player1Grid)
    val playersArray = arrayOf(player1,player2)

    private val shipsSettings = mapOf(
        2 to 1,
        1 to 2
    )


    fun prepare(){
        setUpPlayerShips(player1)
        setUpPlayerShips(player2)

        player1.playerGrid.visualizePlayerGrid()
        player2.playerGrid.visualizePlayerGrid()

    }

    fun autoPrepare(){
        val ship1 = Ship(mutableListOf(Square("A2"),Square("B2")))
        val ship2 = Ship(mutableListOf(Square("C4")))
        val ship3 = Ship(mutableListOf(Square("D2")))
        player1.addShip(ship1)
        player1.addShip(ship2)
        player1.addShip(ship3)

        val ship4 = Ship(mutableListOf(Square("A1"),Square("A2")))
        val ship5 = Ship(mutableListOf(Square("C2")))
        val ship6 = Ship(mutableListOf(Square("D4")))
        player2.addShip(ship4)
        player2.addShip(ship5)
        player2.addShip(ship6)

        player1.playerGrid.visualizePlayerGrid()
        player2.playerGrid.visualizePlayerGrid()

    }

    fun start(){
        var move = 1
        val maxMovesCount = dimensions* dimensions
        var gameOver = false

        while(move <= maxMovesCount && !gameOver){
            for (player in playersArray){
                println("Ходит ${player.name}. Поле боя:")
                player.visualizeGrids()

                var moveIsDone = false
                var result:State
                while (!moveIsDone){
                    val inputSquare = readLine() ?: continue
                    try{
                        result = player.shoot(Square(inputSquare))
                        println("Результат выстрела: $result")
                        if(result==State.DAMAGED || result==State.DESTROYED) {
                            if (player.isEnemyDead()){
                                println("${player.name} выиграл, поздравляем!")
                                gameOver = true
                                break
                            }
                            println("Вы ходите еще раз!")
                            continue
                        }
                        moveIsDone=true
                    }catch (e:IllegalArgumentException){
                        println("Неверно введены координаты, попробуйте еще  раз")
                    }catch (e:IncorrectShotException){
                        println("Вы уже стреляли по этим координатам")
                    }
                }
                if (gameOver) break
            }
        }
    }

    private fun setUpPlayerShips(player: Player){

        println("Добавляем корабли для ${player.name}")

        for ((shipType,shipCount) in shipsSettings){
            for (i in 1..shipCount) {
                println("Введите квадраты для $shipType-палубного корабля через запятую, например, для двухпалобного корабля - A2,B2 ")

                var inputLocationIsCorrect = false
                while(!inputLocationIsCorrect) {
                    val shipLocationInput = readLine()
                    if (shipLocationInput == null || shipLocationInput == ""){
                        println ("Введена пустая строка, попробуйте еще раз")
                        continue
                    }
                    try {
                        val shipLocation = shipLocationInput.split(",").map { Square(it) }.toMutableList()
                        if (shipLocation.size != shipType) throw IncorrectShipLocationException("Некорректное кол-во квадратов корабля")
                        val ship = Ship(shipLocation)
                        player.addShip(ship)
                        inputLocationIsCorrect = true
                    }catch(e: IllegalArgumentException){
                        println("Неверно указан квадрат, ${e.message}")
                    }catch (e:IncorrectShipLocationException){
                        println("Проблемы с координатами корабля, ${e.message}")
                    }
                }
            }
        }
    }
}


fun main(){
    println("Введие имя игрока 1, либо ничего не вводите для дефолтного имени")
    var player1Name = readLine() ?: "Игрок1"
    if (player1Name.trim() == "") player1Name = "Игрок1"

    println("Введие имя игрока 2, либо ничего не вводите для дефолтного имени")
    var player2Name = readLine() ?: "Игрок2"
    if (player1Name.trim() == "") player2Name = "Игрок2"

    val newGame = Game(player1Name,player2Name)
    newGame.prepare()
    newGame.start()
}