package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    var fiveCount = 0
    var fourCount = 0
    var threeCount = 0
    var twoCount = 0
    val pupilsCount = marks.size

    for (mark in marks){
        when (mark){
            2 -> twoCount++
            3 -> threeCount++
            4 -> fourCount++
            5 -> fiveCount++
        }
    }

    println("Отличников - ${fiveCount.toFloat()*100/pupilsCount}%")
    println("Хорошистов - ${fourCount.toFloat()*100/pupilsCount}%")
    println("Троечников - ${threeCount.toFloat()*100/pupilsCount}%")
    println("Двоечников - ${twoCount.toFloat()*100/pupilsCount}%")
}