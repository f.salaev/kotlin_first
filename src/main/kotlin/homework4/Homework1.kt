package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)

    var evenCount = 0
    var oddCount = 0

    for (element in myArray){
        if (element % 2 == 0) evenCount++ else oddCount++
    }

    println("Количество четных элементов: $evenCount. \nКоличество нечетных элементов: $oddCount.")

}
