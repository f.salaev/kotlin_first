package homework7

fun main() {
    try {
        val user = createUser("login", "pass12345678", "pass12345678")
        println("Создан пользователь: логин - ${user.login}, пароль - ${user.password}")
    }
    catch (e: WrongLoginException) {
        println(e.message)
    }
    catch (e: WrongPasswordException){
        println(e.message)
    }

}

//write your function and Exception-classes here

class User(val login: String, val password: String)

class WrongLoginException(message: String): Exception(message)
class WrongPasswordException(message: String): Exception(message)

const val loginSymbolsMaxCount = 20
const val passSymbolMinCount = 10

fun createUser(login: String, password: String, passwordConfirmation: String): User {
    if (login.length > loginSymbolsMaxCount) throw WrongLoginException("Логин более $loginSymbolsMaxCount символов")
    if (password.length < passSymbolMinCount ) throw WrongPasswordException("Длина пароля меньше $passSymbolMinCount символов")
    if (password != passwordConfirmation) throw WrongPasswordException("Пароль и подтверждение не совпадают")

    return User(login, password)
}