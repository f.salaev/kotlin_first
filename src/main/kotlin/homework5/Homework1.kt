package homework5

//write your classes here

class Lion (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("мясо","зебра","телятина");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food)) satience++
    }
}

class Tiger (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("кабан","оленина","рыба");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food)) satience++
    }
}

class Hippo (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("трава","мясо");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food)) satience++
    }
}

class Wolf (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("оленина","кабан","баранина","зайчатина");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food)) satience++
    }
}

class Giraffe (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("листья","трава");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food)) satience++
    }
}

class Elephant (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("листья","трава", "фрукт");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food)) satience++
    }
}

class Chimpanzee (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("фрукт","трава","насекомое");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food)) satience++
    }
}

class Gorilla (val name: String, var height: Int, var weight: Int){
    private val foodPreferences = arrayOf("бамбук","лианы","сельдерей");
    var satience: Int = 0

    fun eat( food: String){
        if (foodPreferences.contains(food))
        {
            satience++
        }
    }
}

fun main(){
    val lion = Lion("Симба", 120, 220)
    val tiger = Tiger ("Шерхан", 110, 250)
    val hippo = Hippo ("Глория", 165, 1400)
    val wolf = Wolf ("Акела", 80, 65)
    val giraffe = Giraffe ("Марти", 580, 1100)
    val elephant = Elephant ("Бизнес", 350,5000)
    val chimpanzee = Chimpanzee ("Чита", 120, 40)
    val gorilla = Gorilla ("Терк", 150, 365)

    val food = "трава"

    lion.eat(food)
    println("Сытость ${lion.name} - ${lion.satience}")

    tiger.eat(food)
    println("Сытость ${tiger.name} - ${tiger.satience}")

    hippo.eat(food)
    println("Сытость ${hippo.name} - ${hippo.satience}")

    wolf.eat(food)
    println("Сытость ${wolf.name} - ${wolf.satience}")

    giraffe.eat(food)
    println("Сытость ${giraffe.name} - ${giraffe.satience}")

    elephant.eat(food)
    println("Сытость ${elephant.name} - ${elephant.satience}")

    chimpanzee.eat(food)
    chimpanzee.eat(food)
    println("Сытость ${chimpanzee.name} - ${chimpanzee.satience}")

    gorilla.eat(food)
    println("Сытость ${gorilla.name} - ${gorilla.satience}")
}