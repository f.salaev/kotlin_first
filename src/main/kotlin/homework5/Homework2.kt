package homework5

fun main() {
    val digit = readLine()!!.toInt()

    println(revertInt(digit))
}

fun revertInt (source: Int): Int {
    var number = source
    var result = 0
    while (number!=0){
        result = result*10 + number%10
        number /= 10
    }
    return result
}